# Website for the SynCoP workshop

This repository contains the code source to deploy the website of the SynCoP
2024 workshop.

# Development

First, a development environment with Docker can be created as follows:

```bash
docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  --publish 4000:4000 \
  -it jekyll/jekyll \
  /bin/bash
```

Inside the container, the dependencies can be installed as follows:

```
bundle install
bundle exec jekyll serve --host 0.0.0.0 --livereload
```

Finally, the site can be accessed from `http://localhost:4000/workshops/syncop24`.

# Configuration

## Workshop information

The basic information of the event is defined in the file `_config.yml`. In
that file, the following information can be set.

- `title`: the title of the event
- `acronym`: the acronym of the event
- `logo`: the path to the logo of the event
- `email`: the e-mail used for contacting the organizers
- `description`: a description about the event
- `timezone`: the time zone of the event
- `conference`: this variable contains the city and the dates of the event
- `hero`: this variable contains the image shown at the top of the site. Also
  it's possible to set the copyright of the image

## Statistics

Some statistics (_e.g.,_ number of participants) can be defined in the file
`_data/stats.yml`.

## Important dates

The dates of the event can be defined in the file `_data/dates.yml`.

## Keynote speakers

The speakers can be defined in the file `_data/keynotes`. The photos of the
speakers must be saved in the folder `assets/images/speakers.yml`.

## Program

The program can be defined in the file `_data/program.yml`

## Committees

The committees can be defined in the file `_data/committees.yml`
