---
layout: default
---

<h3 class="section-heading text-center mb-3">About {{ site.acronym }}</h3>
<div class="section-intro single-col-max mx-auto mb-4">
  This event is an <a href="https://etaps.org/" target="_blank">ETAPS</a>
  workshop organised as a series of invited talks and submitted contributions.
  Everyone is welcome to attend and participate in the discussions. There are no
  formal proceedings.
</div>

<!-- Scientific Objective -->
<h4 class="text-center mb-4">Scientific objective of {{ site.acronym }}</h4>

<div class="single-col-max mx-auto mb-3">
{{ site.acronym }} aims at bringing together researchers working on verification
and parameter synthesis for systems with discrete or continuous parameters, in
which the parameters influence the behaviour of the system in ways that are
complex and difficult to predict. Such problems may arise for real-time, hybrid
or probabilistic systems in a large variety of application domains. The
parameters can be continuous (e.g. timing, probabilities, costs) or discrete
(e.g. number of processes). The goal can be to identify suitable parameters to
achieve desired behaviour, or to verify the behaviour for a given range of
parameter values.

Systems composed of a finite but possibly arbitrary number of identical
components occur everywhere from hardware design (e.g. cache coherence
protocols) to distributed applications (e.g. client-server applications).
Parameterised verification is the task of verifying the correctness of this kind
of systems regardless the number of their components.

</div>

<!-- Topics -->
<h4 class="text-center mb-4">Topics of {{ site.acronym }}</h4>

<div class="single-col-max mx-auto mb-3">
The scientific subject of the workshop covers (but is not limited to) the following areas:

<ul class="list-unstyled text-start d-inline-block mt-4">
  <li><i class="fas fa-check-circle me-2"></i>parameter synthesis,</li>
  <li><i class="fas fa-check-circle me-2"></i>parametric model checking,</li>
  <li><i class="fas fa-check-circle me-2"></i>regular model checking,</li>
  <li><i class="fas fa-check-circle me-2"></i>robustness analysis,</li>
  <li><i class="fas fa-check-circle me-2"></i>parameterised logics, decidability and complexity issues,</li>
  <li><i class="fas fa-check-circle me-2"></i>formalisms such as parametric timed and hybrid automata, parametric time(d) Petri nets, parametric probabilistic (timed) automata, parametric Markov Decision Processes, networks of identical processes,</li>
  <li><i class="fas fa-check-circle me-2"></i>specifications in automata and logic, term and graph rewriting, Petri nets, process algebra, …</li>
  <li><i class="fas fa-check-circle me-2"></i>validation methods via assertional and regular model checking, reachability and coverability decision procedures, abstractions, theorem proving, constraint solving, …</li>
  <li><i class="fas fa-check-circle me-2"></i>interactions between discrete and continuous parameters,</li>
  <li><i class="fas fa-check-circle me-2"></i>tools and applications to hardware design, cache coherence protocols, security and communication protocols, multithreaded and concurrent programs, programs with relaxed memory models, mobile and distributed systems, database languages and systems, biological systems, …</li>
  <!-- <li><i class="fas fa-check-circle me-2"></i></li> -->
</ul>
</div>

<!-- Important dates -->
<h4 class="text-center mb-4">Important dates</h4>

<div class="single-col-max mx-auto mb-3">
<ul class="list-unstyled text-start d-inline-block mt-4">
{% for date in site.data.dates %}
  <li>
    <i class="fa-regular fa-calendar me-2"></i>
    <span><strong>{{ date.summary }}: </strong></span>
    {% if date.entries %}
      {% for entry in date.entries %}
        <!-- get date -->
        {% if entry.date %}
          {% capture t %}
            <time datetime="{{entry.date}}">
              {{ entry.date | date: "%a %d %b %Y"}}
            </time>
          {% endcapture %}
        {% elsif entry.to and entry.from %}
          {% capture t %}
            {% capture from %}
              {% assign fy = entry.from | date: "%Y" %}
              {% assign ty = entry.to 	| date: "%Y" %}
              {% assign fm = entry.from | date: "%b" %}
              {% assign tm = entry.to 	| date: "%b" %}
              {% if fy != ty %}
                {{ entry.from | date: "%a %d %b %Y" }}
              {% elsif fm != tm %}
                {{ entry.from | date: "%a %d %b" }}
              {% else %}
                {{ entry.from | date: "%a %d" }}
              {% endif %}
            {% endcapture %}
            <time datetime="{{entry.from}}">{{ from }}</time> -
            <time datetime="{{entry.to}}">{{ entry.to | date: "%a %d %b %Y"}}</time>
          {% endcapture %}
        {% endif %}
        <span>
          {{ t }}
          {% if entry.zone %}
            <span data-toggle="tooltip" title="Timezone: {{ entry.zone.label }}">
            <span class="glyphicon glyphicon-time"></span>
            </span>
          {% endif %}
        </span>
      {% endfor %}
    {% else %}
      <span>TBD</span>
    {% endif %}
  </li>
{% endfor %}
</ul>
</div>

<!-- Submission info -->
<h4 class="text-center mb-4">Submission</h4>

<div class="single-col-max mx-auto mb-3">
{{ site.acronym }} seeks short abstracts only. Recently published works, ongoing
works, or works under submission are welcome.<br><br>

The page limit is <strong>3 pages (excluding bibliography) single
column</strong>. All accepted abstracts will be made available to the
participants of {{ site.acronym }} but they will not result in referenced
publications.<br><br>

Authors of accepted abstracts will be required to give an informal presentation
during the workshop.<br><br>

Submission will be made in English in PDF format with a simple email at <a href="mailto:{{ site.email }}">{{ site.email }}</a>.

</div>
